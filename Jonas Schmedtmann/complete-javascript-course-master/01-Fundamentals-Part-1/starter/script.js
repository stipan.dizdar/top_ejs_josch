const country = "Croatia";
const continent = "Europe";
let population = 3.8;

const isIsland = false;
let language;

language = "croatian";

const description = `${country} is in ${continent}, and its ${population} people speak ${language}`;

console.log(description);

if (population > 33) {
    console.log(`${country}'s population is above average`)
} else {
    console.log(`${country}'s population is below average`)
}

// Coding Challenge #1 and #2 

let heightMark = 1.69; // 1.88
let heightJohn = 1.95; // 1.76

let weightMark = 78; // 95
let weightJohn = 92; // 85

let bmiJohn = Math.round((weightJohn / heightJohn ** 2) * 10) / 10;
let bmiMark = Math.round((weightMark / heightMark ** 2) * 10) / 10;

const markHigherBMI = bmiMark > bmiJohn;

if (markHigherBMI) {
    console.log(`Mark's BMI (${bmiMark}) is higher than John's (${bmiJohn})!`)
} else {
    console.log(`John's BMI (${bmiJohn}) is higher then Mark's (${bmiMark})`)
}

// LECTURE: Equality Operators: == vs. ===
// const numNeighbours = Number(prompt(`How many neighbour countries does your country have?`));

// if (numNeighbours === 1) console.log(`Only 1 border!`);
// else if (numNeighbours > 1) console.log(`More than 1 border`);
// else (!numNeighbours) console.log(`No borders`);

// LECTURE: Logical Operators

if (population < 50 && language === `english` && !isIsland) console.log(` You should live in ${country} :)`)
else console.log(`${country} does not meet your criteria :(`);

// Coding Challenge #3

const dolphins = [96, 108, 89];
const koalas = [88, 89, 110];

let dolphinScore = 0
for (let i = 0; i < dolphins.length; i++) {
    if (dolphins[i] > koalas[i] && dolphins[i] >= 100) {
        dolphinScore++;
    } else if (dolphins[i] < koalas[i] && koalas[i] >= 100) {
        dolphinScore--;
    }
}
if (dolphinScore == 0) {
    console.log(`Draw!`)
} else if (dolphinScore < 0) {
    console.log(`Koalas win!`)
} else {
    console.log(`Dolphins win!`)
}

// LECTURE: The switch Statement

switch (language) {
    case 'chinese':
    case 'mandarin':
        console.log(`MOST number of native speakers!`);
        break;
    case 'spanish':
        console.log(`"nd place in most number of native speakers`);
        break;
    case 'english':
        console.log(`3rd place`);
        break;
    case 'hindi':
        console.log(`Number 4`);
        break;
    case 'arabic':
        console.log(`5th most spoken language`);
        break;
    default:
        console.log(`Great language too :D`);
};

console.log(`${country}'s population is ${population > 33 ? 'above' : 'below'} average.`);

// Coding Challenge #4

const bill = [275, 40, 430];
for (let i = 0; i < bill.length; i++) {
    const tip = bill[i] >= 50 && bill[i] <= 300 ? bill[i] * 0.15 : bill[i] * 0.2;
    console.log(`The bill was ${bill[i]}, the tip was ${tip}, and the total value ${bill[i] + tip}`)
}