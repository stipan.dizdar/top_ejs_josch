'use strict';

// Coding Challenge #1

const poll = {
  question: 'What is your favourite programming language?',
  options: ['0: JavaScript', '1: Python', '2: Rust', `3: C++`],
  // This generates [0, 0, 0, 0]. More in the next section!
  answers: new Array(4).fill(0),

  registerNewAnswer() {
    this.answers;
    const questionnaire = Number(
      prompt(`What is your favourite programming language?
        0: JavaScript
        1: Python
        2: Rust
        3: C++`)
    );
    // prompt(`${this.question}\n${this.options.join('\n')
    //\n(Write option number)`)
    if (questionnaire == 0 || 1 || 2 || 3) {
      this.answers[questionnaire]++;
    }
    // typeof answer === 'number' && answer < this.answers.length && this.answers[answer]++;
    this.displayResults();
  },

  displayResults(type = String) {
    type
      ? console.log(
          `Poll results are ${this.answers.join(', ') /*toString()*/}`
        )
      : console.log(`${this.answers}`);
  },
};

const pollNewAsnwerFunction = poll.registerNewAnswer.bind(poll);
const answerPollbtn = document.querySelector('.poll');
answerPollbtn.addEventListener('click', pollNewAsnwerFunction);

const data1 = {
  answers: [1, 5, 3, 9, 6, 1],
};
const displayResultsData = poll.displayResults.bind(data1);
console.log(data1.answers.toString());
displayResultsData();
// poll.displayResultsData.call({answers: [5, 2, 3]}, string)

// Coding Challenge #2
console.log('Coding Challenge #2');

(function () {
  const header = document.querySelector('h1');
  header.style.color = 'red';
  document.querySelector('body').addEventListener('click', function () {
    header.style.color = 'blue';
  });
})();
