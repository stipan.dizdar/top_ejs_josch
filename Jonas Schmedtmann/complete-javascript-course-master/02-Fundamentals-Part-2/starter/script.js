'use strict';

// LECTURE: Functions

function describeCountry(country, population, capitalCity) {
    return `${country} has ${population} people and its capital city is ${capitalCity}`
}

const europe1 = describeCountry('Germany', 83, 'B0erlin');
const asia = describeCountry('Japan', 123, 'Tokyo');
const southAmerica = describeCountry('Uruguay', 3.46, 'Montevideo');

// LECTURE: Function Declarations vs. Expressions
function percentageOfWorld1(population) {
    return (population / 7900) * 100;
};

const percentageOfWorld2 = function (population) {
    return (population / 7900) * 100;
};

// LECTURE: Arrow Functions

const percentageOfWorld3 = population => (population / 7900) * 100;

// LECTURE: Functions Calling Other Functions

const describePopulation = (country, population) => `${country} has ${population} millin people which is about ${percentageOfWorld3(population)}% of the world.`

// Coding Challenge #1

const dolphins = [85, 54, 41];
const koalas = [23, 34, 27];

const calcAverage = scores => {
    let average = 0;

    for (let value of scores) {
        average += value;
    }
    return average / scores.length
}

const checkWinner = (dolphins, koalas) => {

    if (calcAverage(dolphins) > calcAverage(koalas) * 2) {
        return `Dolphins win (${calcAverage(dolphins)} : ${calcAverage(koalas)})`
    } else if (calcAverage(dolphins) * 2 < calcAverage(koalas)) {
        return `Koalas win (${calcAverage(koalas)} : ${calcAverage(dolphins)})`
    }
}

console.log(checkWinner(dolphins, koalas));

// LECTURE: Introduction to Arrays

const populations = [3.46, 3.8, 83, 128];

console.log(populations.length === 4);

const pecentageofWorld1 = population => (population / 7900) * 100;

const percentages = []

for (let population of populations) {
    percentages.push(percentageOfWorld1(population));
}
console.log(percentages);

// LECTURE: Basic Array Operations (Methods)

const neighbours = ['Slovenia', 'Hungary', 'Serbia', 'BiH', 'Montenegro', 'Italy'];
neighbours.push('Utopia');
neighbours.pop();
if (neighbours.includes('Germany')) {
    console.log('Probably not a central European country :D');
}

neighbours[neighbours.indexOf('BiH')] = 'NDH';

// Coding Challenge #2

const calcTip = value => value >= 50 && value <= 300 ? value * 0.15 : value * 0.20;

const bills = [125, 555, 44];
const tips = [];
const total = [];

for (let value of bills) {
    tips.push(calcTip(value));
    total.push(calcTip(value) + value);
}

console.log(tips, total)

// LECTURE: Introduction to Objects & LECTURE: Object Methods


const myCountry = {
    country: 'Croatia',
    capital: 'Zagreb',
    language: 'croatian',
    population: '3.8',

    describe: function () {
        return `${this.country} has ${this.population} million ${this.language}-speaking people, ${neighbours.length} neighbouring countries and a capital called ${this.capital}`
    },

    checkIsland: function () {
        return neighbours == 0 ? true : false;
    }

}

// LECTURE: Dot vs. Bracket Notation

console.log(myCountry.describe());
console.log(myCountry.checkIsland());


// Coding Challenge #3

const person1 = {
    fullName: 'Mark Miller',
    mass: 78,
    height: 1.69,

    calcBMI: function () {
        return this.bmiValue = this.mass / this.height ** 2
    }
}

const person2 = {
    fullName: 'John Smith',
    mass: 92,
    height: 1.95,

    calcBMI: function () {
        return this.bmiValue = this.mass / this.height ** 2
    }
}

person1.calcBMI();
person2.calcBMI();

if (person1.bmiValue > person2.bmiValue) {
    console.log(`${person1.fullName}'s BMI (${person1.bmiValue}) is higher than ${person2.fullName}'s (${person2.bmiValue})`)
} else {
    console.log(`${person2.fullName}'s BMI (${person2.bmiValue}) is higher than ${person1.fullName}'s (${person1.bmiValue})`)
}

// LECTURE: Iteration: The for Loop

for (let i = 1; i <= 50; i++) {
    console.log(`Voter number ${i} is currently voting`)
};

// LECTURE: Looping Arrays, Breaking and Continuing

const percentages2 = [];

for (let population of populations) {
    percentages2.push(percentageOfWorld1(population))
};

//LECTURE: Looping Backwards and Loops in Loops

const listOfneighbours = [['Canada', 'Mexico'], ['Spain'], ['Norway', 'Sweden', 'Russia']];

for (let array of listOfneighbours) {
    for (let element of array) {
        console.log(`Neighbour: ${element}`)
    }
}

// LECTURE: The while Loop

const percentages3 = [];
let i = 0;
while (i < populations.length) {
    percentages3.push(percentageOfWorld1(populations[i]))
    i++;
}
console.log(percentages3);

// Coding Challenge #4

const bills4 = [22, 295, 176, 440, 37, 105, 10, 1100, 86, 52];

const tips4 = [];
const totals = [];

for (let el of bills4) {
    tips4.push(calcTip(el));
    totals.push(calcTip(el) + el)
}

console.log(tips4, totals)

console.log(calcAverage(totals))

