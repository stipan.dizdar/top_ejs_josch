'use strict';

// Data needed for a later exercise
const flights =
  '_Delayed_Departure;fao93766109;txl2133758440;11:25+_Arrival;bru0943384722;fao93766109;11:45+_Delayed_Arrival;hel7439299980;fao93766109;12:05+_Departure;fao93766109;lis2323639855;12:30';

// Data needed for first part of the section
const restaurant = {
  name: 'Classico Italiano',
  location: 'Via Angelo Tavanti 23, Firenze, Italy',
  categories: ['Italian', 'Pizzeria', 'Vegetarian', 'Organic'],
  starterMenu: ['Focaccia', 'Bruschetta', 'Garlic Bread', 'Caprese Salad'],
  mainMenu: ['Pizza', 'Pasta', 'Risotto'],

  openingHours: {
    thu: {
      open: 12,
      close: 22,
    },
    fri: {
      open: 11,
      close: 23,
    },
    sat: {
      open: 0, // Open 24 hours
      close: 24,
    },
  },
};

// Coding Challenge #1

const game = {
  team1: 'Bayern Munich',
  team2: 'Borrussia Dortmund',
  players: [
    [
      'Neuer',
      'Pavard',
      'Martinez',
      'Alaba',
      'Davies',
      'Kimmich',
      'Goretzka',
      'Coman',
      'Muller',
      'Gnarby',
      'Lewandowski',
    ],
    [
      'Burki',
      'Schulz',
      'Hummels',
      'Akanji',
      'Hakimi',
      'Weigl',
      'Witsel',
      'Hazard',
      'Brandt',
      'Sancho',
      'Gotze',
    ],
  ],
  score: '4:0',
  scored: ['Lewandowski', 'Gnarby', 'Lewandowski', 'Hummels'],
  date: 'Nov 9th, 2037',
  odds: {
    team1: 1.33,
    x: 3.25,
    team2: 6.5,
  },
  printGoals: function (...args) {
    let i = 0;
    for (let name of args) {
      i++;
      console.log(name);
    }
    return i;
  },
};

const [players1, players2] = game.players;
// game.team1 = { gk: players1[0], fieldPLayers: [...players1] };
// game.team2 = { gk: players2[0], fieldPLayers: [...players2] };
const allPLayers = [...players1, ...players2];
const players1Final = [...players1, 'Thiago', 'Couthino', 'Perisic'];
const {
  odds: { team1, x: draw, team2 },
} = game;

// console.log(game.printGoals(...game.scored));
// console.log(game.odds.team1 < game.odds.team2 && game.odds.team1);
// console.log(game);

// Coding Challenge #2
for (const el of game.scored.entries()) {
  console.log(`Goal ${el[0] + 1}: ${el[1]}`);
}

let oddsAverage = 0;
for (const odd of Object.values(game.odds)) {
  oddsAverage += odd;
}
oddsAverage /= Object.values(game.odds).length;

console.log(oddsAverage);

for (const [key, value] of Object.entries(game.odds)) {
  const victory = game[key] ? `victory of ${game[key]}` : 'of draw';
  console.log(`Odd ${victory}: ${value}`);
}

// Bonus
const scorers = {};

// game.scored
const whoScored = ['Lewandowski', 'Gnarby', 'Lewandowski', 'Hummels'];

// for (let i = 0; i < whoScored.length; i++) {
//   if (whoScored[i] in scorers) {
//     scorers[whoScored[i]]++;
//   } else {
//     scorers[whoScored[i]] = 1;
//   }
// }

for (const player of game.scored) {
  scorers[player] ? scorers[player]++ : (scorers[player] = 1);
}
console.log(scorers);

// Coding Challenge #3

const gameEvents = new Map([
  [17, '⚽ GOAL'],
  [36, '🔁 Substitution'],
  [47, '⚽ GOAL'],
  [61, '🔁 Substitution'],
  [64, '🔶 Yellow card'],
  [69, '🔴 Red card'],
  [70, '🔁 Substitution'],
  [72, '🔁 Substitution'],
  [76, '⚽ GOAL'],
  [80, '⚽ GOAL'],
  [92, '🔶 Yellow card'],
]);
const events = new Set();
// const events = [...new set(gameEvents.values())]
for (const [key, value] of gameEvents) {
  events.add(value);
  const gameStr = key < 45 ? `[FIRST HALF]` : `[SECOND HALF]`;
  console.log(`${gameStr} ${key}: ${value}`);
}
gameEvents.delete(64);
console.log(events);
console.log(`An event happened, on average, every ${90 / gameEvents.size}`);

// Coding Challenge #4
document.body.append(document.createElement('textarea'));
document.body.append(document.createElement('button'));

const input = document.querySelector('textarea');
const btnTag = document.querySelector('button');

btnTag.addEventListener('click', toCamelCase);

function toCamelCase() {
  const arr = input.value
    .toLowerCase()
    .split('\n'); /* destructure [prva, druga] = input.value... */
  let i = 1;
  let emoji = '✅';
  for (let str of arr) {
    /* [i, str] arr.entries()*/
    const strArr = str.trim().split('_');
    strArr[1] = strArr[1].replace(strArr[1][0], strArr[1][0].toUpperCase());
    // strArr[1] = strArr[1][0].toUpperCase() + strArr[1].slice(1);
    const strCl = [strArr[0], strArr[1]]
      .join('')
      .padEnd(25, ' ')
      .concat(emoji.repeat(i)); /* i+1 od entries() */
    console.log(strCl);
    i++;
  }
}
