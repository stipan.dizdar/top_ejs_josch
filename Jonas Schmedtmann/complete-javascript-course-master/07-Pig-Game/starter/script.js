'use strict';

const newGameBtn = document.querySelector('.btn--new');
const diceRoll = document.querySelector('.btn--roll');
const dicePic = document.querySelector('.dice');
const player1 = document.querySelector('.player--0');
const player2 = document.querySelector('.player--1');
const holdBtn = document.querySelector('.btn--hold');
const curScore = document.querySelectorAll('.current-score');
const scr = document.querySelectorAll('.score');
let currentScoreBox = 0;

const newGame = () => {
  for (let i = 0; i < scr.length; i++) {
    scr[i].textContent = 0;
    curScore[i].textContent = 0;
  }
  currentScoreBox = 0;
};
const activePLayer = () => {
  player1.classList.toggle('player--active');
  player2.classList.toggle('player--active');
  currentScoreBox = 0;
};

const holdHook = () => {
  document.querySelector('.player--active .score').textContent =
    Number(document.querySelector('.player--active .score').textContent) +
    Number(
      document.querySelector('.player--active .current-score').textContent
    );
  if (
    Number(document.querySelector('.player--active .score').textContent) >= 100
  ) {
    document.querySelector('.player--active').classList.add('player--winner');
    newGame();
  }
  activePLayer();
};

const rollDice = () => {
  const diceNum = Math.trunc(Math.random() * 6) + 1;
  dicePic.setAttribute('src', `dice-${diceNum}.png`);
  if (diceNum === 1) {
    document.querySelector('.player--active .current-score').textContent = 0;
    activePLayer();
  } else {
    currentScoreBox += diceNum;
    document.querySelector(
      '.player--active .current-score'
    ).textContent = currentScoreBox;
  }
};

diceRoll.addEventListener('click', rollDice);
newGameBtn.addEventListener('click', newGame);
holdBtn.addEventListener('click', holdHook);
