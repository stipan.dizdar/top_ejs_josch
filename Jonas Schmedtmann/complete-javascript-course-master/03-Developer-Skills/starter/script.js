// Remember, we're gonna use strict mode in all scripts now!
'use strict';

// Coding Challenge #1

const data1 = [17, 21, 23];
const data2 = [12, 5, -5, 0, 4];

const printForecast = function (arr) {
  let tempFore = '... ';
  for (let i = 0; i < arr.length; i++) {
    tempFore += `${arr[i]}°C in ${i + 1} days... `;
  }
  return tempFore;
};
console.log(printForecast(data1));
console.log(printForecast(data2));
