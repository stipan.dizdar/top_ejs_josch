'use strict';

// Coding Challenge #1
const Car = function (make, speed) {
  (this.make = make), (this.speed = speed);
};

Car.prototype.accelerate = function () {
  this.speed += 10;
  console.log(this.speed);
};
Car.prototype.brake = function () {
  this.speed = this.speed - 5;
  console.log(this.speed);
};
const bmw = new Car('BMW', 120);
const mercedes = new Car('Mercedes', 95);

// Coding Challenge #2

class CarCl {
  constructor(make, speed) {
    (this.make = make), (this.speed = speed);
  }
  accelerate() {
    this.speed += 10;
    console.log(this.speed);
    return this;
  }
  brake() {
    this.speed -= 5;
    console.log(this.speed);
    return this;
  }
  get speedUS() {
    return this.speed / 1.6;
  }
  set speedUS(speed) {
    this.speed = speed * 1.6;
  }
}
const ford = new CarCl('Ford', 120);

// console.log(ford.accelerate());
// console.log(ford.speedUS);
// ford.speedUS = 50;
// console.log(ford);

// Coding Challenge #3

const EV = function (speed, make, charge) {
  Car.call(this, make, speed);
  this.charge = charge;
};
EV.prototype = Object.create(Car.prototype);
EV.prototype.chargeBattery = function (chargeTo) {
  this.charge = chargeTo;
};

EV.prototype.accelerate = function () {
  this.charge -= 1;
  this.speed += 20;
  console.log(`${this.make} going at ${this.speed}
    km/h, with a charge of ${this.charge}%`);
};

const tesla = new EV(120, 'Tesla', 23);

class EVCl extends CarCl {
  #charge;
  constructor(make, speed, charge) {
    super(make, speed);
    this.#charge = charge;
  }

  chargeBattery = function (chargeTo) {
    this.charge = chargeTo;
    return this;
  };

  accelerate = function () {
    this.#charge -= 1;
    this.speed += 20;
    console.log(`${this.make} going at ${this.speed}
      km/h, with a charge of ${this.#charge}%`);
    return this;
  };
}

const rivian = new EVCl('Rivian', 120, 23);

rivian.accelerate().chargeBattery(90).brake();
console.log(rivian.speedUS);
