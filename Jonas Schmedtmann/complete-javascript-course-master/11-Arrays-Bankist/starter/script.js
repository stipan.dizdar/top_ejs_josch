'use strict';

/////////////////////////////////////////////////
/////////////////////////////////////////////////
// BANKIST APP

// Data
const account1 = {
  owner: 'Jonas Schmedtmann',
  movements: [200, 450, -400, 3000, -650, -130, 70, 1300],
  interestRate: 1.2, // %
  pin: 1111,
};

const account2 = {
  owner: 'Jessica Davis',
  movements: [5000, 3400, -150, -790, -3210, -1000, 8500, -30],
  interestRate: 1.5,
  pin: 2222,
};

const account3 = {
  owner: 'Steven Thomas Williams',
  movements: [200, -200, 340, -300, -20, 50, 400, -460],
  interestRate: 0.7,
  pin: 3333,
};

const account4 = {
  owner: 'Sarah Smith',
  movements: [430, 1000, 700, 50, 90],
  interestRate: 1,
  pin: 4444,
};

const accounts = [account1, account2, account3, account4];

// Elements
const labelWelcome = document.querySelector('.welcome');
const labelDate = document.querySelector('.date');
const labelBalance = document.querySelector('.balance__value');
const labelSumIn = document.querySelector('.summary__value--in');
const labelSumOut = document.querySelector('.summary__value--out');
const labelSumInterest = document.querySelector('.summary__value--interest');
const labelTimer = document.querySelector('.timer');

const containerApp = document.querySelector('.app');
const containerMovements = document.querySelector('.movements');

const btnLogin = document.querySelector('.login__btn');
const btnTransfer = document.querySelector('.form__btn--transfer');
const btnLoan = document.querySelector('.form__btn--loan');
const btnClose = document.querySelector('.form__btn--close');
const btnSort = document.querySelector('.btn--sort');

const inputLoginUsername = document.querySelector('.login__input--user');
const inputLoginPin = document.querySelector('.login__input--pin');
const inputTransferTo = document.querySelector('.form__input--to');
const inputTransferAmount = document.querySelector('.form__input--amount');
const inputLoanAmount = document.querySelector('.form__input--loan-amount');
const inputCloseUsername = document.querySelector('.form__input--user');
const inputClosePin = document.querySelector('.form__input--pin');

let currentAccount;

btnLogin.addEventListener('click', function (e) {
  e.preventDefault();

  currentAccount = accounts.find(acc => acc.user === inputLoginUsername.value);
  console.log(currentAccount);

  if (currentAccount?.pin === Number(inputLoginPin.value)) {
    labelWelcome.textContent = `Welcome back, ${
      currentAccount.owner.split(' ')[0]
    }`;
    containerApp.style.opacity = 100;
    inputLoginUsername.value = inputLoginPin.value = '';
    updateUI(currentAccount);
  }
});

btnTransfer.addEventListener('click', function (e) {
  e.preventDefault();
  const amount = Number(inputTransferAmount.value);
  const receiverAcc = accounts.find(acc => acc.user === inputTransferTo.value);
  inputTransferAmount.value = inputTransferTo.value = '';
  if (
    amount > 0 &&
    receiverAcc &&
    currentAccount.balance >= amount &&
    receiverAcc?.user !== currentAccount.user
  ) {
    currentAccount.movements.push(-amount);
    receiverAcc.movements.push(amount);
  }
  updateUI(currentAccount);
});

btnLoan.addEventListener('click', function (e) {
  e.preventDefault();
  const amount = Number(inputLoanAmount.value);
  if (amount > 0 && currentAccount.movements.some(mov => mov >= amount / 10)) {
    currentAccount.movements.push(amount);
    updateUI(currentAccount);
  }
  inputLoanAmount.value = '';
});
btnClose.addEventListener('click', function (e) {
  e.preventDefault();

  if (
    inputCloseUsername.value === currentAccount.user &&
    Number(inputClosePin.value) === currentAccount.pin
  ) {
    const index = accounts.findIndex(acc => acc.user === currentAccount.user);

    accounts.splice(index, 1);
    containerApp.style.opacity = 0;
  }
  inputCloseUsername.value = inputClosePin.value = '';
});

let sorted = false;
btnSort.addEventListener('click', function (e) {
  e.preventDefault();
  displayMovements(currentAccount.movements, !sorted);
  sorted = !sorted;
});
/////////////////////////////////////////////////
/////////////////////////////////////////////////
// LECTURES

const currencies = new Map([
  ['USD', 'United States dollar'],
  ['EUR', 'Euro'],
  ['GBP', 'Pound sterling'],
]);

const movements = [200, 450, -400, 3000, -650, -130, 70, 1300];

// movements.forEach(function (el, i) {
//   const div = document.createElement('div');
//   div.classList.add('movements__value');
//   const typeDiv = document.createElement('div');
//   typeDiv.classList.add('movements__type');
//   const dateDiv = document.createElement('div');
//   dateDiv.classList.add('movements__date');
//   const valueDiv = document.createElement('div');
//   valueDiv.classList.add('movements__value');
//   valueDiv.innerText = `${el}`;
//   const withDep = el > 0 ? 'DEPOSIT' : 'WITHDRAVAL';
//   typeDiv.innerText = `${i} ${withDep}`;
//   div.appendChild(typeDiv);
//   div.appendChild(dateDiv);
//   div.appendChild(valueDiv);
//   el < 0
//     ? div.classList.add('movements__type--withdrawal')
//     : div.classList.add('movements__type--deposit');
//   containerMovements.appendChild(div);
// });
const displayMovements = function (movements, sort = false) {
  containerMovements.innerHTML = '';

  const movs = sort ? movements.slice().sort((a, b) => a - b) : movements;
  movs.forEach(function (el, i) {
    const type = el > 0 ? 'deposit' : 'withdrawal';
    const html = ` <div class="movements__row">
      <div class="movements__type movements__type--${type}">${
      i + 1
    } ${type}</div>
      <div class="movements__date">3 days ago</div>
      <div class="movements__value">${el}€</div>`;
    containerMovements.insertAdjacentHTML('afterbegin', html);
  });
};

const calcDisplayBalance = function (acc) {
  acc.balance = acc.movements.reduce((curr, mov) => curr + mov, 0);
  labelBalance.textContent = `${acc.balance} €`;
};

const calcDisplaySummary = function (acc) {
  const incomes = acc.movements
    .filter(mov => mov > 0)
    .reduce((acc, curr) => acc + curr, 0);
  labelSumIn.textContent = `${incomes}€`;
  const out = acc.movements
    .filter(mov => mov < 0)
    .reduce((acc, curr) => acc + curr, 0);
  labelSumOut.textContent = `${Math.abs(out)}€`;

  const interest = movements
    .filter(mov => mov > 0)
    .map(deposit => (deposit * currentAccount.interestRate) / 100)
    .filter((int, i, arr) => int >= 1)
    .reduce((acc, int) => acc + int, 0);
  labelSumInterest.textContent = `${interest}€`;
};

const createUsernames = function (accs) {
  accs.forEach(function (acc) {
    acc.user = acc.owner
      .toLowerCase()
      .split(' ')
      .map(name => name[0])
      .join('');
  });
};
createUsernames(accounts);

const updateUI = function (acc) {
  displayMovements(acc.movements);
  calcDisplayBalance(acc);
  calcDisplaySummary(acc);
};
/////////////////////////////////////////////////

// Working With Arrays

// Coding Challenge #1

const dogsJulia = [3, 5, 2, 12, 7];
const dogsKate = [4, 1, 15, 8, 3];
const noCatsJulia = dogsJulia.splice(1, -2);
const checkDogs = function (array) {
  array.forEach((el, i) => {
    const isPuppy =
      i > 2 ? `an adult, and is ${el} years old` : `is stil a puppy`;
    console.log(`Dog number ${i + 1} is ${isPuppy}`);
  });
};
console.log([...noCatsJulia, ...dogsKate]);
checkDogs([...noCatsJulia, ...dogsKate]);

// Coding Challenge #2 -- Coding Challenge #3

const calcAverageAge = dogs =>
  console.log(
    dogs
      .map(dog => (dog < 3 ? 2 * dog : 16 + dog * 4))
      .filter(dog => dog > 17)
      .reduce((sum, curr, i, arr) => sum + curr / arr.length, 0)
  );

calcAverageAge([5, 2, 4, 1, 15, 8, 3]);

const dice = Array.from({ length: 100 }, (_, i) =>
  Math.round(Math.random() * 100 + 1)
);

console.log(dice);

// Coding Challenge #4

const dogs = [
  { weight: 22, curFood: 250, owners: ['Alice', 'Bob'] },
  { weight: 8, curFood: 200, owners: ['Matilda'] },
  { weight: 13, curFood: 275, owners: ['Sarah', 'John'] },
  { weight: 32, curFood: 340, owners: ['Michael'] },
];
// forEach
dogs.map(dog => (dog.recommendedFood = dog.weight ** 0.75 * 28));
const sarahDog = dogs.find(owner => owner.owners.includes('Sarah'));
console.log(sarahDog.recommendedFood > sarahDog.curFood);

const ownersEatTooMuch = [],
  ownersEatTooLittle = [];
// filter(() => {})
dogs.map(owner => {
  owner.recommendedFood > owner.curFood
    ? ownersEatTooLittle.push(...owner.owners)
    : ownersEatTooMuch.push(...owner.owners);
});
console.log(ownersEatTooLittle, ownersEatTooMuch);

// ${owners.join(' and ')}
console.log(
  `${ownersEatTooLittle[0]} and ${ownersEatTooLittle[1]} and ${ownersEatTooLittle}'s dogs are eating to little`
);
console.log(
  `${ownersEatTooMuch[0]} and ${ownersEatTooMuch[1]} and ${ownersEatTooMuch}'s dogs are eating to much`
);
console.log(dogs.some(dogs => dogs.recommendedFood === dogs.curFood));
console.log(
  dogs.some(
    dogs =>
      dogs.curFood > dogs.recommendedFood * 0.9 &&
      dogs.curFood < dogs.recommendedFood * 1.1
  )
);
const okay = dogs
  .slice()
  .filter(
    dogs =>
      dogs.curFood > dogs.recommendedFood * 0.9 &&
      dogs.curFood < dogs.recommendedFood * 1.1
  );

console.log(okay);
const orderedDogs = [...dogs];
console.log(orderedDogs.sort((a, b) => a.recommendedFood - b.recommendedFood));
