'use strict';

let secretNumber = Math.trunc(Math.random() * 20) + 1;
let score = 20;
let highscore = document.querySelector('.highscore').textContent;

const resetBtn = function () {
  document.querySelector('.guess').value = '';
  displayMessage('Start guessing...');
  document.querySelector('.score').textContent = 20;
  document.querySelector('body').style.backgroundColor = '#222';
  secretNumber = Math.trunc(Math.random() * 20) + 1;
  document.querySelector('.number').textContent = '?';
};

const displayMessage = function (message) {
  document.querySelector('.message').textContent = message;
};

const numberGuessing = function () {
  const playerNumber = document.querySelector('.guess').value;

  const messageToPlayer = [
    'No number!',
    'You lost!',
    'Correct number!',
    'Too high!',
    'Too low!',
  ];
  if (!playerNumber) {
    displayMessage(messageToPlayer[0]);
  }

  if (playerNumber != secretNumber) {
    score--;
    document.querySelector('.score').textContent = score;
    if (score === 0) {
      displayMessage(messageToPlayer[1]);
    } else {
      document.querySelector('.message').textContent =
        playerNumber > secretNumber ? messageToPlayer[3] : messageToPlayer[4];
    }
  } else if (playerNumber == secretNumber) {
    displayMessage(messageToPlayer[2]);
    if (score > highscore) {
      document.querySelector('.highscore').textContent = score;
      highscore = document.querySelector('.highscore').textContent;
      document.querySelector('body').style.backgroundColor = '#60b347';
      document.querySelector('.number').textContent = secretNumber;
    }
    document.querySelector('.btn.again').addEventListener('click', resetBtn);
  }
};

document.querySelector('.btn.check').addEventListener('click', numberGuessing);
